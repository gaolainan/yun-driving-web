import Vue from 'vue'
import uView from 'uview-ui';
Vue.use(uView);
import App from './App'
import globalConst from 'utils/global.js';

Vue.prototype.globalConst = globalConst;
Vue.config.productionTip = false

App.mpType = 'app'

if (uni.getSystemInfoSync().platform !== "devtools") {
	console.log = () => {}
}

const app = new Vue({
    ...App
})
app.$mount()
