import request from 'utils/request.js'

const api = {}

//用户信息api接口
api.login=(code) => request.post('/user/login?code='+code)

//用户信息api接口
api.getUserInfo=(token) => request.get('/user/info?token='+token)

//用户修改用户信息api接口
api.updateInfo=(data) => request.put('/user',data)

//获取时间段
api.gettimeorder=(type) => request.get('/time_order/list?type='+type)

//更新用户信息
api.updateUserinfo=(data) => request.put('/user',data)

//获取一周的排班列表
api.getArrageList=(time,pass,type) => request.get('/arrangement/list?mondayTime='+time+'&pass='+pass+'&type='+type)

//获得所有的用户 （根据分校的id）  目前不太行
api.getAllUserInfo=(branchId,college) => request.get('/user/list?branchId='+branchId+'&college='+college)

// 根据分校得id拿到所有的用户
api.getAllUserInfoByBranchId=(branchId) => request.get('/user/list?branchId='+branchId)

//排班预约
api.postArrangement=(arrangement) => request.post('/arrangement/arrangement',arrangement)

//排班预约
api.postAppointment=(appointment) => request.post('/arrangement/appointment', appointment)

// 根据arragementId删除
api.deleteUserFromArrangementById=(arrangementId) => request.delete('/arrangement?arrangementId=',arrangementId)

//获取项目列表
api.getmoduleList=(branchId) => request.get('/module/list?branchId='+branchId)

// 获取所有分校信息
api.getBranchList=() => request.get('/branch/list')

// 获取所有分校信息
api.getRoleList=() => request.get('/role/list')

// 根据分校得id拿到所有的用户
api.getAllUserInfoByBranchAndRole=(branchId, roleId, page, pageSize) => request.get('/user/list?branchId='+branchId + '&roleId=' + roleId + '&page=' + page + '&pageSize=' + pageSize)

// 修改用户角色
api.updateUserRole=(userId, roleId) => request.put('/user/role?userId='+userId + '&roleId=' + roleId)

// 管理员发布通知
api.postAnnounce=(data) => request.post('/notification',data)

// 获取用户所在分校的教练列表
api.getUserCoachList=() => request.get('/user/list/user_branch_coach')

// 获取差评列表
api.getSettingValue=(settingKey) => request.get('/setting?settingKey=' + settingKey)

// 添加评价
api.addEvaluate=(data) => request.post('/evaluate', data)

// 通过教练的Id查询对应的评价
api.getCocahEvaluateById=(coachId) => request.get('/evaluate/coach?coachId='+coachId)

// 获取排班记录列表
api.getArrangementRecordList=(type) => request.get('/arrangement/list/record?type=' + type)

//获取申请排班是否弹窗
api.getSubject2_3Status=(type) => request.put('/user/subject2_3?type=' + type)

// 意见反馈
api.postFeedback=(data) => request.post('/feedback/send_feedback', data)


// 获得当前用户的通知
api.getNowUserNotice=(status) => request.get('/notification/currentUser?readStatus='+status)

// 根据分校id 类型 返回列表通知
api.getNoticeList=(branchId, type) => request.get('/notification/list?branchId='+branchId+'&type='+type)

// 通知已读
api.changeNoticeReadStatus=(data) => request.put('/notification',data)

// 获得当前排班列表的时间段
api.getTimeOrder=(type) => request.get('/time_order/list?type='+type)

// 删除选定时间段的排班
api.deleteTimeOrder=(timeOrderId) => request.delete('/time_order?timeOrderId=',timeOrderId)

// 增加排版的时间区间
api.addTimeOrder=(data) => request.post('/time_order',data)

// 更改时间区间起始和结束
api.changeTimeOrder=(data) => request.put('/time_order',data)

// 通过key拿到value
api.getSettingByKey=(key) => request.get("/setting?settingKey="+key)

// 增加key和value
api.addSettingValueByKey=(data) => request.post('/setting',data)


// 查询通过或不通过的排班
api.getUnAllowArrangement=(time,type,pass) => request.get('/arrangement/list?mondayTime='+time+'&type='+type+'&pass='+pass)


// 通过排班
api.allowArrangement=(data) => request.put('/arrangement',data)

// 删除排班
api.deleteArrangement=(id) => request.delete('/arrangement?arrangementId=',id)

// 修改setting
api.updateSetting=(data) => request.put('/setting', data)

// 删除setting
api.deleteSetting=(settingId) => request.delete('/setting?settingId=', settingId)

// 获取分校的列表
api.getBranchList=() => request.get('/branch/list')

// 获得所有的模块
api.getAllModules=() => request.get('/module/list')

// 增加模块
api.addModule=(data) => request.post('/module',data)

// 删除模块
api.deleteModule=(moduleId) => request.delete('/module?moduleId=',moduleId)


// 删除主页通知
api.deleteIndexNotice=(notificationId) => request.delete('/notification?notificationId=',notificationId)

// 根据分校的id 获取当前分校的成员的信息
api.checkUsersInfo=(branchId) => request.get('/user/list?branchId='+branchId)

// 根据分校id  删除分校
api.deleteBranchById=(id) => request.delete('/branch/delete?id=',id)

// 增加分校
api.addDifferentBranch=(data) => request.post('/branch/add',data)

// 删除消息
api.deleteMessageById=(id) => request.delete("/notification?notificationId=",id)

api.getRotationChart = (priority)=>request.get("/rotation_chart?priority="+priority)

api.deleteRotationChart = (id) =>request.delete("/rotation_chart?rotationChartId=",id)

api.addRotationChart = (data) =>request.post("/rotation_chart",data)

api.uploadImage = (data) =>request.post("/rotation_chart/upload",data)

//更新分校信息
api.updateBranchInfo=(data) => request.put('/branch/update',data)

// 根据用户姓名或手机号模糊查询
api.getAllUserInfoByNameOrPhone=(keyword) => request.get('/user/list?realName='+keyword + '&phone=' + keyword)

api.getpasscount = (type) =>request.get('/user/pass_count?type='+type)

api.getunreadnews = (id) =>request.get('/notification/branch/threeSubject?branchId='+id)


// 根据分校id 拿到对应的轮播图
api.getRotationChartImageByBranchId = (branchId) => request.get("/rotation_chart/branch?priority="+branchId)

// 获得对应的未处理的排版或者预约
api.getUnArrageAndAppointmentList = (branchId, pass, type) => request.get("/arrangement/list/branchId?branchId="+branchId+"&pass="+pass+"&type="+type)


// 获得所有校区的信息
api.getAllBranchList = () => request.get("/branch/list")

// 获得指定校区的信息
api.getAllNotificationByBranchId = (branchId) => request.get("/evaluate/branch/coach?branchId="+branchId)

// 获得指定教练的相关评价
api.getAllNotificationByCoachId = (coachId) => request.get("/evaluate/coach?coachId="+coachId)

// 获得指定分校的教练
api.getAllCoachListByBranchId = (branchId) => request.get("/user/list/branchId?branchId="+branchId)


// 获得对应的差评

export default api