import api from 'utils/api.js'

const WXlogin = {}

WXlogin.wxlogin = () => {
	var that = this
	 wx.login({
		success: (res) => {
		api.login(res.code).then((res)=>{
			if(res.code===0){
				uni.setStorageSync('user', res.data);
				uni.setStorageSync('token', res.data.token);
				api.getUserInfo(uni.getStorageSync('token')).then((res)=>{
					if(res.data===0){
						uni.setStorageSync('userVO', res.data);
					}
				})
				
				uni.switchTab({
					url: "/pages/index/index"
				})
			}
		})
		}
	})
}

export default WXlogin